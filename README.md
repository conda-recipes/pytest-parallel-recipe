# pytest-parallel conda recipe

Home: https://github.com/browsertron/pytest-parallel

Package license: MIT

Recipe license: BSD 3-Clause

Summary: conda recipe for conda package of pytest-parallel
